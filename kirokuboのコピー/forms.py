from django import forms
from .models import Article, Category, Tag, Importance,Member

#Articleのフォーム
class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['member','cate','importance','title','content','memo']
        # widgets = {
        #     'importance': forms.RadioSelect()
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'
        # self.fields['importance'].widget = forms.RadioSelect()

# Editフォーム
class ArticleEditForm(forms.ModelForm):
    class Mata:
        model = Article
        fields = ['member', 'cate', 'importance', 'title', 'content', 'memo']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'