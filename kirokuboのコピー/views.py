from django.shortcuts import render
from django.shortcuts import redirect
from .models import Article,Category,Tag,Importance,Member
from .forms import ArticleForm
from .forms import ArticleEditForm

def index(request):
    data = Article.objects.all()
    params = {
        # 'message':'post前',
        'ArticleForm': ArticleForm(),
        'data':data,
    }

    if (request.method == 'POST'):
        params = { 'message':'post後'}
        obj = Article()
        article = ArticleForm(request.POST, instance=obj)
        article.save()
        return redirect(to='/kirokubo')

    return render(request, 'kirokubo/index.html', params)

def edit(request, num):
    obj = Article.objects.get(id=num)

    if (request.method == 'POST'):
        article = ArticleForm(request.POST, instance=obj)
        article.save()
        return redirect(to='/kirokubo')

    params = {
        # 'message':'post前',
        'id':num,
        'form': ArticleForm(instance=obj),
    }

    return render(request, 'kirokubo/edit.html', params)

