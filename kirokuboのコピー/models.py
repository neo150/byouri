from django.db import models
# カテゴリークラス
class Category(models.Model):
    category_name = models.CharField(
        verbose_name="カテゴリー名",
        max_length=255
        )
    category_detail = models.TextField()

    def __str__(self):
        return str(self.category_name)

# 名前クラス
class Name(models.Model):
    name_list = (
        ('新田','新田'),
        ('山形','山形'),
        ('ユズリハ','ユズリハ'),
        ('三谷','三谷'),       
    )
    name = models.CharField(max_length=100,choices=name_list)


# 職員クラス
class Member(models.Model):
    member_name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.member_name)

# タグクラス
class Tag(models.Model):
    tag_name = models.CharField(max_length=255)
    tag_detail = models.TextField()
 
    def __str__(self):
        return str(self.tag_name)


#　重要度クラス
class Importance(models.Model):
    importance_level = models.IntegerField(default=1)

    def __str__(self):
        return str(self.importance_level)


# 記事クラス
class Article(models.Model):
    title = models.CharField(
        verbose_name="タイトル",
        max_length=255,
        blank=True, 
        null=True,
        )
    content = models.TextField(
        verbose_name='詳細',
        blank=True, 
        null=True,
        )
    created_at = models.DateTimeField(
        verbose_name='日時',
        auto_now_add=True,
        )

    updated_at = models.DateTimeField(
        auto_now_add= True,
        blank = True,
        null = True,
    )

    memo = models.TextField(
        verbose_name= "メモ",
        blank = True,
        null = True,
    )

    cate = models.ForeignKey(
        Category,on_delete=models.CASCADE
        )
    importance = models.ForeignKey(
        Importance, on_delete=models.CASCADE
        )
    
    member = models.ForeignKey(
        Member, on_delete=models.CASCADE
    )

    def __str__(self):
        return str(self.id) +':'+ str(self.title) + '(' + str(self.created_at) + ')'

    class Meta:
        ordering = ('-created_at',)

