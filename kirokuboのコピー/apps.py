from django.apps import AppConfig


class KirokuboConfig(AppConfig):
    name = 'kirokubo'
