from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='process_index'),
    path('uketuke', views.uketuke, name='uketuke'),
    path('uketuke_bio', views.uketuke_bio, name='uketuke_bio'),
    path('uketuke_op', views.uketuke_op, name='uketuke_op'),
    path('uketuke_opLN', views.uketuke_opLN, name='uketuke_opLN'),
    path('kiridasi',views.kiridasi,name='kiridasi'),
    path('kiridasi_LN',views.kiridasi_LN,name='kiridasi_LN'),
    path('kiridasi_copy',views.kiridasi_copy,name='kiridasi_copy'),
    path('houmai',views.houmai,name='houmai'),
    path('houmai_LN',views.houmai_LN,name='houmai_LN'),
    path('teisyutu',views.teisyutu,name='process_teisyutu'),
    path('sindan',views.sindan,name='sindan'),
    path('detail/<int:num>',views.detail,name='process_detail'),
    path('find',views.find,name='process_find'),
    path('list',views.list,name='process_list'),
    # path('edit/<int:num>', views.edit, name='edit'),
    path('listview', views.ProcessListView.as_view(), name='listview'),
]