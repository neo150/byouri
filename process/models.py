from django.db import models
from datetime import datetime
# コメント詳細
# class Comment(models.Model):
#     contents = models.TextField(
#         verbose_name='コメント内容',
#         blank=True,
#         null = True,
#     )

# プロセスクラス
class Process(models.Model):
    bango = models.CharField(
        verbose_name='標本番号',
        max_length=255,
        unique=True,
    )

    status = models.CharField(
        verbose_name= 'ステイタス',
        max_length=255,
        default='受付済',
    )

    zairyou_list = (
        ('食道','食道'),
        ('胃','胃'),
        ('十二指腸','十二指腸'),
        ('小腸','小腸'),
        ('虫垂','虫垂'),
        ('大腸','大腸'),
        ('直腸','直腸'),
        ('肛門','肛門'),
        ('大網','大網'),
        ('小網','小網'),
        ('腸間膜','腸間膜'),
        ('腹膜','腹膜'),
        ('肝臓','肝臓'),
        ('胆管','胆管'),
        ('胆嚢','胆嚢'),
        ('膵臓','膵臓'),
        ('膵管','膵管'),
        ('気管','気管'),
        ('気管支','気管支'),
        ('肺','肺'),
        ('胸膜','胸膜'),
        ('縦隔','縦隔'),
        ('胸腺','胸腺'),
        ('子宮','子宮'),
        ('子宮頚部','子宮頚部'),
        ('子宮体部','子宮体部'),
        ('子宮内容物','子宮内容物'),
        ('卵管','卵管'),
        ('卵巣','卵巣'),
        ('胎盤','胎盤'),
        ('膀胱','膀胱'),
        ('前立腺','前立腺'),
        ('尿管','尿管'),
        ('尿道','尿道'),
        ('腎臓','腎臓'),
        ('副腎','副腎'),
        ('精巣','精巣'),
        ('皮膚','皮膚'),
        ('皮下組織','皮下組織'),
        ('脂肪組織','脂肪組織'),
        ('口腔','口腔'),
        ('唾液腺','唾液腺'),
        ('咽頭','咽頭'),
        ('喉頭','喉頭'),
        ('鼻腔','鼻腔'),
        ('リンパ節','リンパ節'),
        ('骨髄','骨髄'),
        ('脾臓','脾臓'),
        ('心臓','心臓'),
        ('弁','弁'),
        ('心筋','心筋'),
        ('動脈','動脈'),
        ('乳腺','乳腺'),
        ('甲状腺','甲状腺'),
        ('膵臓','膵臓'),
        ('脳','脳'),
        ('髄膜','髄膜'),
        ('骨','骨'),
    )
    zairyou = models.CharField(
        verbose_name='材料名',
        max_length=255,
        choices=zairyou_list,
        null=True,
        blank=True,
    )

    saisyu_list = (
        ('生検','生検'),
        ('手術','手術'),
        ('LN','LN'),
    )
    saisyu = models.CharField(
        verbose_name='採取名',
        max_length=50,
        choices=saisyu_list,
        null=True,
        blank=True,
    )

    # created_at = models.DateTimeField(
    #     verbose_name ='作成日時',
    #     auto_now_add = True,
    # )

    uketuke_at = models.DateTimeField(
        verbose_name ='受付済',
        # blank=True, 
        # null=True,
        # default=datetime.now(),
    )

    kiridasi_at = models.DateTimeField(
        verbose_name = '切り出し済',
        blank=True, 
        null=True,
    )

    houmai_at = models.DateTimeField(
        verbose_name = '包埋済',
        blank=True, 
        null=True,
    )

    teisyutu_at = models.DateTimeField(
        verbose_name = '提出済',
        blank=True, 
        null=True,
    )

    sindan_at = models.DateTimeField(
        verbose_name = '診断済',
        blank=True, 
        null=True,
    )

    comment = models.TextField(
        verbose_name='コメント',
        blank=True,
        null=True,
    )

    sikyu = models.BooleanField(
        verbose_name='至急',
        blank=True,
        null=True,
    )

    # comment = models.ForeignKey(
    #     Comment, on_delete=models.CASCADE,
    #     blank=True, 
    #     null=True,
    # )

    def __str__(self):
        return str(self.bango) + '(' + str(self.uketuke_at) +')'

# 受付から今日までの日数
    def diff(self):
        date = self.uketuke_at.date()
        diff = date.today()-date
        return diff.days

# 受付から診断終了までの日数（TAT）
    def tat_diff(self):
        # uketuke_date = self.uketuke_at.date()
        # sindan_date = self.sindan_at.date()
        if self.status == "診断済":
            tat_diff = self.sindan_at.date()-self.uketuke_at.date()
            return tat_diff.days
        else:
            # tat_diff = self.sindan_at.date()-self.uketuke_at.date()
            # return tat_diff.days
            return ''

# 受付日時自動入力
    def save(self, *args, **kwargs):
        if not self.id:
            self.uketuke_at = datetime.now()
        return super(Process, self).save(*args, **kwargs)


    class Meta:
        ordering = ('bango',)

