from django.shortcuts import render
from .models import Process
from .forms import UketukeForm
from .forms import BangoForm
from .forms import DetailForm
from django.shortcuts import redirect
from datetime import datetime
from datetime import date
from django.views.generic import ListView

# import qrcode
# Create your views here.
class ProcessListView(ListView):
    model = Process
    # template_name = ".html"


def index(request):
    # data = Process.objects.all()
    uketuke_data = Process.objects.filter(status='受付済')
    uketuke_data_count = Process.objects.filter(status='受付済').count()
    uketuke_biopsy_count = Process.objects.filter(status='受付済').filter(saisyu='生検').count()
    uketuke_op_count = Process.objects.filter(status='受付済').filter(saisyu='手術').count()
    kiridasi_data = Process.objects.filter(status='切り出し済')
    kiridasi_data_count = Process.objects.filter(status='切り出し済').count()
    kiridasi_biopsy_count = Process.objects.filter(status='切り出し済').filter(saisyu='生検').count()
    kiridasi_op_count = Process.objects.filter(status='切り出し済').filter(saisyu='手術').count()
    houmai_data = Process.objects.filter(status='包埋済')
    houmai_data_count = Process.objects.filter(status='包埋済').count()
    houmai_biopsy_count = Process.objects.filter(status='包埋済').filter(saisyu='生検').count()
    houmai_op_count = Process.objects.filter(status='包埋済').filter(saisyu='手術').count()
    teisyutu_data = Process.objects.filter(status='提出済')
    teisyutu_data_count = Process.objects.filter(status='提出済').count()
    teisyutu_biopsy_count = Process.objects.filter(status='提出済').filter(saisyu='生検').count()
    teisyutu_op_count = Process.objects.filter(status='提出済').filter(saisyu='手術').count()

# QRコード作成
    # img = qrcode.make('てすとです')
    # img.save('process/static/process/img/qr_code.png')

    params = {
        'uketuke_data':uketuke_data,
        'uketuke_data_count':uketuke_data_count,
        'uketuke_biopsy_count':uketuke_biopsy_count,
        'uketuke_op_count':uketuke_op_count,
        'kiridasi_data':kiridasi_data,
        'kiridasi_data_count':kiridasi_data_count,
        'kiridasi_biopsy_count':kiridasi_biopsy_count,
        'kiridasi_op_count':kiridasi_op_count,
        'houmai_data':houmai_data,
        'houmai_data_count':houmai_data_count,
        'houmai_biopsy_count':houmai_biopsy_count,
        'houmai_op_count':houmai_op_count,
        'teisyutu_data':teisyutu_data,
        'teisyutu_data_count':teisyutu_data_count,
        'teisyutu_biopsy_count':teisyutu_biopsy_count,
        'teisyutu_op_count':teisyutu_op_count,
    }
    return render(request, 'process/index.html', params)

def detail(request,num):
    obj = Process.objects.get(id=num)

    if (request.method == 'POST'):
        process = DetailForm(request.POST, instance=obj)
        process.save()
        return redirect(to='/process')

    params = {
        # 'data':'post前',
        'id':num,
        'DetailForm': DetailForm(instance=obj),
    }

    return render(request, 'process/detail.html', params)

def find(request):
    if Process.objects.filter(bango=request.POST['bango']).exists():
        obj = Process.objects.get(bango=request.POST['bango'])
        params = {
            # 'data':'post前',
            'id':obj.id,
            'DetailForm': DetailForm(instance=obj),
        }
        return render(request, 'process/detail.html', params)

    else:
        return redirect(to='/process')

def uketuke(request):
    today_uketuke_data = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).order_by('-uketuke_at')
    today_uketuke_data_count = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).count()

    params = {
        'data':'けんすうは４０',
        'today_uketuke_data_count':today_uketuke_data_count,
        'today_uketuke_data':today_uketuke_data,
        'UketukeForm':UketukeForm(),
    }

    formset = UketukeForm(request.POST)

    # if (request.method == 'POST' and formset.is_valid()):
    #     obj = Process()
    #     process = UketukeForm(request.POST, instance=obj)
    #     # process.uketuke_at = datetime.now()
    #     process.save()
    #     redirect(to='/process/uketuke')

# /で区切った文字列をリスト化して、それぞれ追加
    if (request.method == 'POST' and formset.is_valid()):
        bango_str = request.POST['bango']
        bango_list = bango_str.split('/')
        for x in bango_list:
            process = Process()
            process.bango = x
            # process.uketuke_at = datetime.now()
            process.save()
        redirect(to='/process/uketuke')

    return render(request, 'process/uketuke.html',params)

def uketuke_bio(request):
    today_uketuke_data = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).order_by('-uketuke_at')
    today_uketuke_data_count = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).count()

    params = {
        'data':'けんすうは４０',
        'today_uketuke_data_count':today_uketuke_data_count,
        'today_uketuke_data':today_uketuke_data,
        'UketukeForm':UketukeForm(),
    }

    formset = UketukeForm(request.POST)

    if (request.method == 'POST' and formset.is_valid()):
        process = Process()
        # request.POST['saisyu'] = '生検'
        # process = UketukeForm(request.POST, instance=obj)
        process.bango = request.POST['bango']
        process.saisyu = '生検'
        process.save()
        redirect(to='/process/uketuke_bio')

    return render(request, 'process/uketuke_bio.html',params)

def uketuke_op(request):
    today_uketuke_data = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).order_by('-uketuke_at')
    today_uketuke_data_count = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).count()

    params = {
        'data':'けんすうは４０',
        'today_uketuke_data_count':today_uketuke_data_count,
        'today_uketuke_data':today_uketuke_data,
        'UketukeForm':UketukeForm(),
    }

    formset = UketukeForm(request.POST)

    if (request.method == 'POST' and formset.is_valid()):
        process = Process()
        # request.POST['saisyu'] = '生検'
        # process = UketukeForm(request.POST, instance=obj)
        process.bango = request.POST['bango']
        process.saisyu = '手術'
        process.save()
        redirect(to='/process/uketuke_op')

    return render(request, 'process/uketuke_op.html',params)

# OPの受付と同時にリンパ節用の子番号を発行
def uketuke_opLN(request):
    today_uketuke_data = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).order_by('-uketuke_at')
    today_uketuke_data_count = Process.objects.filter(status='受付済').filter(uketuke_at__icontains=date.today()).count()

    params = {
        'data':'けんすうは４０',
        'today_uketuke_data_count':today_uketuke_data_count,
        'today_uketuke_data':today_uketuke_data,
        'UketukeForm':UketukeForm(),
    }

    formset = UketukeForm(request.POST)

    if (request.method == 'POST' and formset.is_valid()):
        process = Process()
        # request.POST['saisyu'] = '生検'
        # process = UketukeForm(request.POST, instance=obj)
        process.bango = request.POST['bango']
        process.saisyu = '手術'
        process.save()

        # リンパ節番号を発行
        process = Process()
        process.bango = request.POST['bango']+'-LN'
        process.saisyu = 'LN'
        process.save()

        redirect(to='/process/uketuke_opLN')

    return render(request, 'process/uketuke_opLN.html',params)

# 切り出し分割（先行で行くものをコピー）
def kiridasi_copy(request):
    pre_data = Process.objects.filter(status='受付済')
    pre_data_count = Process.objects.filter(status='受付済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).order_by('-kiridasi_at')
    today_data_count = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
      'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='受付済').filter(bango=request.POST['bango']).exists()):
            # 元のデータを取得 さらに　コメントに分割の日時を追加
            moto_process = Process.objects.get(bango=request.POST['bango'])
            moto_process.comment = moto_process.comment + str(datetime.today().strftime("%Y/%m/%d/ %H:%M:%S"))+'に分割'

            # 新しいデータを作り、それに元の受け付け日時を入れる　病理番号の後に今日の日付を追加
            process = Process()
            process.bango = moto_process.bango +'-'+str(datetime.today().strftime("%Y%m%d"))
            process.uketuke_at = moto_process.uketuke_at
            process.saisyu = moto_process.saisyu
            process.zairyou = moto_process.zairyou
            process.kiridasi_at = datetime.now()
            process.status = '切り出し済'
            process.comment = moto_process.comment
            process.save()
            redirect(to='/process/kiridasi_copy')
        else:
            redirect(to='/process/kiridasi_copy')           


    return render(request, 'process/kiridasi_copy.html',params)

def kiridasi(request):
    pre_data = Process.objects.filter(status='受付済')
    pre_data_count = Process.objects.filter(status='受付済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).order_by('-kiridasi_at')
    today_data_count = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
      'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='受付済').filter(bango=request.POST['bango']).exists()):
            process = Process.objects.get(bango=request.POST['bango'])
            process.kiridasi_at = datetime.now()
            process.status = '切り出し済'
            process.save()
            redirect(to='/process/kiridasi')
        else:
            redirect(to='/process/kiridasi')           


    return render(request, 'process/kiridasi.html',params)

def kiridasi_LN(request):
    pre_data = Process.objects.filter(status='受付済').filter(saisyu='LN')
    # pre_data_count = Process.objects.filter(status='受付済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).order_by('-kiridasi_at')
    today_data_count = Process.objects.filter(status='切り出し済').filter(kiridasi_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
    #   'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='受付済').filter(bango=request.POST['bango']+'-LN').exists()):
            process = Process.objects.get(bango=request.POST['bango']+'-LN')
            process.kiridasi_at = datetime.now()
            process.status = '切り出し済'
            process.save()
            redirect(to='/process/kiridasi_LN')
        else:
            redirect(to='/process/kiridasi_LN')           


    return render(request, 'process/kiridasi_LN.html',params)

def houmai(request):
    pre_data = Process.objects.filter(status='切り出し済')
    pre_data_count = Process.objects.filter(status='切り出し済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='包埋済').filter(houmai_at__icontains=date.today()).order_by('-houmai_at')
    today_data_count = Process.objects.filter(status='包埋済').filter(houmai_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
      'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='切り出し済').filter(bango=request.POST['bango']).exists()):
            process = Process.objects.get(bango=request.POST['bango'])
            process.houmai_at = datetime.now()
            process.status = '包埋済'
            process.save()
            redirect(to='/process/houmai')
        else:
            redirect(to='/process/houmai')           

    return render(request, 'process/houmai.html',params)

def houmai_LN(request):
    pre_data = Process.objects.filter(status='切り出し済').filter(saisyu='LN')
    # pre_data_count = Process.objects.filter(status='切り出し済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='包埋済').filter(houmai_at__icontains=date.today()).order_by('-houmai_at')
    today_data_count = Process.objects.filter(status='包埋済').filter(houmai_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
    #   'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='切り出し済').filter(bango=request.POST['bango']).exists()):
            process = Process.objects.get(bango=request.POST['bango'])
            process.houmai_at = datetime.now()
            process.status = '包埋済'
            process.save()
            redirect(to='/process/houmai_LN')
        else:
            redirect(to='/process/houmai_LN')           

    return render(request, 'process/houmai_LN.html',params)

def teisyutu(request):
    pre_data = Process.objects.filter(status='包埋済')
    pre_data_count = Process.objects.filter(status='包埋済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='提出済').filter(houmai_at__icontains=date.today()).order_by('-teisyutu_at')
    today_data_count = Process.objects.filter(status='提出済').filter(houmai_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
      'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='包埋済').filter(bango=request.POST['bango']).exists()):
            process = Process.objects.get(bango=request.POST['bango'])
            process.teisyutu_at = datetime.now()
            process.status = '提出済'
            process.save()
            redirect(to='/process/teisyutu')
        else:
            redirect(to='/process/teisyutu')           

    return render(request, 'process/teisyutu.html',params)

def sindan(request):
    pre_data = Process.objects.filter(status='提出済')
    pre_data_count = Process.objects.filter(status='提出済').count()
    # count = Process.objects.filter(status='切り出し済').count()
    today_data = Process.objects.filter(status='診断済').filter(sindan_at__icontains=date.today()).order_by('-sindan_at')
    today_data_count = Process.objects.filter(status='診断済').filter(sindan_at__icontains=date.today()).count()
    params = {
      'pre_data':pre_data,
      'pre_data_count':pre_data_count,
    #   'data':count,
      'BangoForm':BangoForm(),
      'today_data':today_data,
      'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if(Process.objects.filter(status='提出済').filter(bango=request.POST['bango']).exists()):
            process = Process.objects.get(bango=request.POST['bango'])
            process.sindan_at = datetime.now()
            process.status = '診断済'
            process.save()
            redirect(to='/process/sindan')
        else:
            redirect(to='/process/sindan')           

    return render(request, 'process/sindan.html',params)

def list(request):
    data = Process.objects.all()
    data_count = Process.objects.count()
    # count = Process.objects.filter(status='切り出し済').count()
    # today_data = Process.objects.filter(status='診断済').filter(sindan_at__icontains=date.today()).order_by('-sindan_at')
    # today_data_count = Process.objects.filter(status='診断済').filter(sindan_at__icontains=date.today()).count()
    params = {
      'data':data,
      'data_count':data_count,
    #   'data':count,
    #   'BangoForm':BangoForm(),
    #   'today_data':today_data,
    #   'today_data_count':today_data_count,
      }

    if (request.method == 'POST'):
        if Process.objects.filter(bango=request.POST['bango']).exists():
            obj = Process.objects.get(bango=request.POST['bango'])
            params = {
                # 'data':'post前',
                'id':obj.id,
                'DetailForm': DetailForm(instance=obj),
            }
            return render(request, 'process/detail.html', params)
        else:
            return redirect(to='/process/list.html')        

    return render(request, 'process/list.html',params)