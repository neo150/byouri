from django import forms
from .models import Process

# 受付フォーム
class UketukeForm(forms.ModelForm):
    class Meta:
        model = Process
        fields = [
            'bango',
            # 'status',
        ]
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
        # self.fields['bango'].widget.attrs['id'] = 'autofocus'


# 切り出しフォーム
class BangoForm(forms.ModelForm):
    class Meta:
        model = Process
        fields = [
            'bango',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
        # self.fields['end_at'].widget.attrs['id'] = 'endbox'

# 詳細フォーム
class DetailForm(forms.ModelForm):
    class Meta:
        model = Process
        fields = [
            'sikyu',
            'zairyou',
            'comment',
            'uketuke_at',
            'kiridasi_at',
            'houmai_at',
            'teisyutu_at',
            'sindan_at',
            'saisyu',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'
        # self.fields['end_at'].widget.attrs['id'] = 'endbox'
