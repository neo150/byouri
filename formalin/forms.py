from django import forms
from .models import Formalin

# # 受付フォーム
# class UketukeForm(forms.ModelForm):
#     class Meta:
#         model = Process
#         fields = [
#             'bango',
#             # 'status',
#         ]
    
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in self.fields.values():
#             field.widget.attrs['class'] = 'form-control'
#         # self.fields['bango'].widget.attrs['id'] = 'autofocus'


# # 切り出しフォーム
# class BangoForm(forms.ModelForm):
#     class Meta:
#         model = Process
#         fields = [
#             'bango',
#         ]

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in self.fields.values():
#             field.widget.attrs['class'] = 'form-control'
#         # self.fields['end_at'].widget.attrs['id'] = 'endbox'

class NyukoForm(forms.ModelForm):
    class Meta:
        model = Formalin
        fields = [
            'bango',
        ]


# 詳細フォーム
class DetailForm(forms.ModelForm):
    class Meta:
        model = Formalin
        fields = [
            'status',
            'kigen_at',
            'nyuko_at',
            'haraidasi',
            'haraidasi_at',
            'teisyutu',
            'teisyutu_at',
            'teisyutu_at',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'
        # self.fields['end_at'].widget.attrs['id'] = 'endbox'
