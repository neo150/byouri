from django.shortcuts import render
from .models import Formalin
from django.db.models import Count
# from .forms import UketukeForm
# from .forms import BangoForm
from .forms import DetailForm
from django.shortcuts import redirect
from datetime import datetime
from datetime import date

def index(request):

    nyuko_data = Formalin.objects.filter(status='入庫済')
    kigengire_data = Formalin.objects.filter(status='入庫済').filter(kigen_at__lt=datetime.now())
    haraidasi_kigengire_data = Formalin.objects.filter(status='払い出し中').filter(kigen_at__lt=datetime.now())
    haraidasi_data = Formalin.objects.filter(status='払い出し中')
    op_haraidasi_data = Formalin.objects.filter(status='払い出し中').filter(haraidasi='OP')
    # haraidasi_data = Formalin.objects.filter(status='提出')
    today_teisyutu_data = Formalin.objects.filter(status='提出済').filter(teisyutu_at__icontains=date.today())

    # セッションでグループバイ 今日の提出個数
    teisyutu_groupby_data = today_teisyutu_data.values('ima','teisyutu').annotate(total=Count('teisyutu')).order_by('-ima')

    # 払い出し先でグループバイ 各部署の払い出し個数
    haraidasi_groupby_data = haraidasi_data.values('haraidasi').annotate(total=Count('haraidasi')).order_by('haraidasi')

    params = {
        'nyuko_data':nyuko_data,
        'kigengire_data':kigengire_data,
        'haraidasi_kigengire_data':haraidasi_kigengire_data,
        'haraidasi_data':haraidasi_data,
        'op_haraidasi_data':op_haraidasi_data,
        'today_teisyutu_data':today_teisyutu_data,
        'teisyutu_groupby_data':teisyutu_groupby_data,
        'haraidasi_groupby_data':haraidasi_groupby_data,
    }
    return render(request, 'formalin/index.html', params)

def list(request):

    all_data = Formalin.objects.all().order_by('status')

    params = {
        'all_data':all_data,
    }
    return render(request, 'formalin/list.html', params)

# 詳細表示（編集もかねて）
def detail(request,num):
    obj = Formalin.objects.get(id=num)

    if (request.method == 'POST'):
        formalin = DetailForm(request.POST, instance=obj)
        formalin.save()
        return redirect(to='/formalin')

    params = {
        # 'data':'post前',
        'id':num,
        'DetailForm': DetailForm(instance=obj),
    }

    return render(request, 'formalin/detail.html', params)


# 検索処理
def find(request):
    if Formalin.objects.filter(bango=request.POST['bango']).exists():
        obj = Formalin.objects.get(bango=request.POST['bango'])
        params = {
            # 'data':'post前',
            'id':obj.id,
            'DetailForm': DetailForm(instance=obj),
        }
        return render(request, 'formalin/detail.html', params)

    else:
        return redirect(to='/formalin')

def nyuko(request):
    # 使用期限をセッションに保存して使う

    if (request.method == 'GET'):
        request.session['kigen']=request.GET['kigen']
        request.session['ima']=str(datetime.now())
        
    if (request.method == 'POST'):
        if(Formalin.objects.filter(status='入庫済').filter(bango=request.POST['bango']).exists()==False):
            formalin = Formalin()
            formalin.ima = request.session['ima']
            formalin.kigen_at = request.session['kigen']
            formalin.bango = request.POST['bango']
            formalin.status = '入庫済'
            formalin.save()
            redirect('nyuko')

# すべての入庫データ
    nyuko_data = Formalin.objects.filter(status='入庫済')

# 今回入庫したもののみ
    ima_nyuko_data = Formalin.objects.filter(status='入庫済').filter(ima = request.session['ima'])

    params = {
        'ima':request.session['ima'],
        'kigen': request.session['kigen'],
        'nyuko_data': nyuko_data,
        'ima_nyuko_data':ima_nyuko_data,
    }

    return render(request,'formalin/nyuko.html',params)

def nyuko_ikkatu(request):
    # 入庫一括登録 最初と最後の番号
    # 使用期限をセッションに保存して使う

    if (request.method == 'GET'):
        request.session['kigen']=request.GET['kigen']
        request.session['ima']=str(datetime.now())
        
    if (request.method == 'POST'):
        start = int(request.POST['bango_start'])
        end = int(request.POST['bango_end'])+1
        # 登録は200以内 (入力間違い防止)
        if end-start < 200:
            for i in range(start, end):
                if(Formalin.objects.filter(status='入庫済').filter(bango=str(i)).exists()==False):
                    formalin = Formalin()
                    formalin.ima = request.session['ima']
                    formalin.kigen_at = request.session['kigen']
                    formalin.bango = str(i)
                    # formalin.bango = request.POST['bango']
                    formalin.status = '入庫済'
                    formalin.save()
        redirect('nyuko')

# すべての入庫データ
    nyuko_data = Formalin.objects.filter(status='入庫済')

# 今回入庫したもののみ
    ima_nyuko_data = Formalin.objects.filter(status='入庫済').filter(ima = request.session['ima'])

    params = {
        'ima':request.session['ima'],
        'kigen': request.session['kigen'],
        'nyuko_data': nyuko_data,
        'ima_nyuko_data':ima_nyuko_data,
    }

    return render(request,'formalin/nyuko.html',params)


def haraidasi(request):
    # 部署をセッションに保存して使う
    if (request.method == 'GET'):
        request.session['busyo']=request.GET['busyo']
        request.session['ima']=str(datetime.now())

    if (request.method == 'POST'):
        if(Formalin.objects.filter(status='入庫済').filter(bango=request.POST['bango']).exists()):
            formalin = Formalin.objects.get(bango=request.POST['bango'])
            formalin.ima = request.session['ima']
            formalin.haraidasi_at = datetime.now()
            formalin.haraidasi = request.session['busyo']
            formalin.status = '払い出し中'
            formalin.save()
            redirect('haraidasi')
        else:
            redirect('haraidasi')   

    nyuko_data = Formalin.objects.filter(status='入庫済')
    haraidasi_data = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo'])
    ima_haraidasi_data = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).filter(ima = request.session['ima'])
    haraidasi_data_count = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).count()
    today_haraidasi_data = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).filter(haraidasi_at__icontains=date.today()).order_by('-haraidasi_at')
    today_haraidasi_data_count = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).filter(haraidasi_at__icontains=date.today()).count()
    params = {
        'busyo': request.session['busyo'],
        'nyuko_data': nyuko_data,
        'haraidasi_data':haraidasi_data,
        'ima_haraidasi_data':ima_haraidasi_data,
        'haraidasi_data_count':haraidasi_data_count,
        'today_haraidasi_data_count':today_haraidasi_data_count,
        'today_haraidasi_data':today_haraidasi_data,
    }

    return render(request,'formalin/haraidasi.html',params)


def teisyutu(request):
    # 部署をセッションに保存して使う
    if (request.method == 'GET'):
        request.session['busyo']=request.GET['busyo']
        request.session['ima']=str(datetime.now())

    if (request.method == 'POST'):
        if(Formalin.objects.filter(status='払い出し中').filter(bango=request.POST['bango']).exists()):
            formalin = Formalin.objects.get(bango=request.POST['bango'])
            formalin.teisyutu_at = datetime.now()
            formalin.ima = request.session['ima']
            formalin.teisyutu = request.session['busyo']
            formalin.status = '提出済'
            formalin.save()
            redirect('teisyutu')
        else:
            redirect('teisyutu')   

    nyuko_data = Formalin.objects.filter(status='入庫済')
    haraidasi_data = Formalin.objects.filter(status='払い出し中')
    today_haraidasi_data = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).filter(haraidasi_at__icontains=date.today()).order_by('-haraidasi_at')
    today_haraidasi_data_count = Formalin.objects.filter(status='払い出し中').filter(haraidasi=request.session['busyo']).filter(haraidasi_at__icontains=date.today()).count()
    ima_teisyutu_data = Formalin.objects.filter(status='提出済').filter(teisyutu=request.session['busyo']).filter(ima = request.session['ima'])
    teisyutu_data = Formalin.objects.filter(status='提出済').filter(teisyutu=request.session['busyo'])
    params = {
        'busyo': request.session['busyo'],
        'nyuko_data': nyuko_data,
        'haraidasi_data':haraidasi_data,
        'today_haraidasi_data_count':today_haraidasi_data_count,
        'today_haraidasi_data':today_haraidasi_data,
        'ima_teisyutu_data':ima_teisyutu_data,
    }

    return render(request,'formalin/teisyutu.html',params)

