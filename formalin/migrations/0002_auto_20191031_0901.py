# Generated by Django 2.2.6 on 2019-10-31 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formalin', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formalin',
            name='kigen_at',
            field=models.DateField(verbose_name='使用期限'),
        ),
    ]
