from django.apps import AppConfig


class FormalinConfig(AppConfig):
    name = 'formalin'
