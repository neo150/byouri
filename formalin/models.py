from django.db import models
from datetime import datetime

# ホルマリンクラス
class Formalin(models.Model):
# 登録時の時間をセッションで記録し、取り出すため
    ima = models.CharField(
        verbose_name='セッション日時',
        max_length=255,
        blank=True, 
        null=True,
    )

    bango = models.CharField(
        verbose_name='バーコード番号',
        max_length=255,
        unique=True,
    )

    status = models.CharField(
        verbose_name= 'ステイタス',
        max_length=255,
        default='入庫済',
    )

    kigen_at = models.DateField(
        verbose_name ='使用期限',
    )

    created_at = models.DateTimeField(
        verbose_name ='作成日時',
        auto_now_add = True,
    )

    updated_at = models.DateTimeField(
        verbose_name ='更新日時',
        auto_now = True,
    )

    nyuko_at = models.DateTimeField(
        verbose_name ='入庫日時',
        blank=True, 
        null=True,
    )

    haraidasi_at = models.DateTimeField(
        verbose_name ='払い出し日時',
        blank=True, 
        null=True,
    )

    haraidasi = models.CharField(
        verbose_name= '払い出し先',
        max_length=255,
        blank=True, 
        null=True,
    )

    teisyutu_at = models.DateTimeField(
        verbose_name ='提出日時',
        blank=True, 
        null=True,
    )
    
    teisyutu = models.CharField(
        verbose_name= '提出先',
        max_length=255,
        blank=True, 
        null=True,
    )

    def __str__(self):
        return str(self.bango)

# 受付日時自動入力
    def save(self, *args, **kwargs):
        if not self.id:
            self.nyuko_at = datetime.now()
        return super(Formalin, self).save(*args, **kwargs)


    class Meta:
        ordering = ('bango',)