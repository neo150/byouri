from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('nyuko', views.nyuko, name='nyuko'),
    path('nyuko_ikkatu', views.nyuko_ikkatu, name='nyuko_ikkatu'),
    path('haraidasi', views.haraidasi, name='haraidasi'),
    path('teisyutu', views.teisyutu, name='teisyutu'),
    # path('nyuko/<kigen>', views.nyuko, name='nyuko'),
    # path('uketuke', views.uketuke, name='uketuke'),
    # path('uketuke_bio', views.uketuke_bio, name='uketuke_bio'),
    # path('uketuke_op', views.uketuke_op, name='uketuke_op'),
    # path('kiridasi',views.kiridasi,name='kiridasi'),
    # path('houmai',views.houmai,name='houmai'),
    # path('teisyutu',views.teisyutu,name='teisyutu'),
    # path('sindan',views.sindan,name='sindan'),
    path('detail/<int:num>',views.detail,name='detail'),
    path('find',views.find,name='find'),
    path('list',views.list,name='list'),
    # path('edit/<char:num>', views.edit, name='edit'),
]