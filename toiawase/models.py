from django.db import models
from django.core import validators

class Cate(models.Model):
    # item = models.ForeignKey(Item,on_delete=models.CASCADE)
    title = models.CharField(
        verbose_name='カテゴリー名',
        max_length=200,
        blank=True,
        null=True,
    )
    
    description = models.TextField()
    
    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'カテゴリ'
        verbose_name_plural = 'カテゴリー'

class Saki(models.Model):
    # item = models.ForeignKey(Item,on_delete=models.CASCADE)
    title = models.CharField(
        verbose_name='問い合わせ先',
        max_length=200,
        blank=True,
        null=True,
    )
    description = models.TextField()
    
    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = '問い合わせ先'
        verbose_name_plural = '問い合わせ先'

class Item(models.Model):

    SAKI_CHOICES = (
        (1, '外来'),
        (2, '病棟'),
        (3, 'BML'),
        (4, '会計'),
        (5, '検査'),
        (6, 'その他'),
    )

    CATEGORY_CHOICES = (
        (1, '結果問い合わせ'),
        (2, '会計問い合わせ'),
        (3, '連絡'),
        (4, '業者'),
        (5, '検査'),
        (6, 'その他'),
    )

    name = models.CharField(
        verbose_name='問い合わせ名前',
        max_length=200,
        blank=True,
        null=True,

    )
    # age = models.IntegerField(
    #     verbose_name='問い合わせ先',
    #     validators=[validators.MinValueValidator(1)],
    #     # blank=True,
    #     null=True,
    # )
    category = models.IntegerField(
        verbose_name='カテゴリ',
        choices=CATEGORY_CHOICES,
        # blank=True,
        null=True,
    )

    cate = models.ForeignKey(Cate,on_delete=models.CASCADE)
    # toisaki = models.ForeignKey(Saki,on_delete=models.CASCADE)

    saki = models.IntegerField(
        verbose_name='問い合わせ先',
        choices=SAKI_CHOICES,
        # blank=True,
        null=True,
    )

    naiyou = models.TextField(
        verbose_name='内容',
        max_length=300,
        blank=True,
        null=True,
    )

    tag = models.TextField(
        verbose_name='タグ',
        max_length=300,
        blank=True,
        null=True,
    )

    created_at = models.DateTimeField(
        verbose_name='登録日',
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name='更新日',
        auto_now=True
    )

    # 管理サイト上の表示設定
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name = 'アイテム'
        verbose_name_plural = 'アイテム'

