from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse

from django.views.generic.edit import CreateView

from .models import Item
from .models import Cate
from .forms import ItemForm

def list_toiawase(request):
    return HttpResponse("hello world")

def index(request):
    cate_list = Cate.objects.all()
    item_list = Item.objects.order_by('-created_at')
    item_count = Item.objects.count()
    # item_list = Item.objects.all()
    context = {
        'item_list':item_list,
        'title':'一覧表示',
        'cate_list':cate_list,
        'item_count':item_count,
        'form':ItemForm(),
        }
    return render(request, 'toiawase/list_toiawase.html',context)

class ItemCreateView(CreateView):
    model = Item
    form_class = ItemForm
    success_url = '/toiawase'



