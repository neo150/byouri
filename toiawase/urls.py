from django.urls import path
from . import views
from .views import ItemCreateView

urlpatterns = [
    path('', views.index, name = 'list_toiawase'),
    path('create/', ItemCreateView.as_view(), name='create')
]