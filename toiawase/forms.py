from django import forms
from .models import Item

class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ('name', 'cate','naiyou')
        widgets = {
            'name':forms.TextInput(),
            'cate':forms.RadioSelect(),
        } 