from django.apps import AppConfig


class ToiawaseConfig(AppConfig):
    name = 'toiawase'
