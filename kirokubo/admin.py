from django.contrib import admin
from .models import Tag, Article

# admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Article)
# admin.site.register(Importance)
# admin.site.register(Member)
