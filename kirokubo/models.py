from django.db import models
from datetime import datetime
from django.utils import timezone

# カテゴリークラス
class Category(models.Model):
    category_name = models.CharField(
        verbose_name="カテゴリー名",
        max_length=255
        )
    category_detail = models.TextField()

    def __str__(self):
        return str(self.category_name)

# 名前クラス
# class Name(models.Model):
#     name_list = (
#         ('新田','新田'),
#         ('山形','山形'),
#         ('ユズリハ','ユズリハ'),
#         ('三谷','三谷'),       
#     )
#     name = models.CharField(max_length=100,choices=name_list)


# 職員クラス
class Member(models.Model):
    member_name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.member_name)

# タグクラス
class Tag(models.Model):
    tag_name = models.CharField(max_length=255)
    tag_detail = models.TextField()
 
    def __str__(self):
        return str(self.tag_name)


#　重要度クラス
# class Importance(models.Model):
#     importance_level = models.CharField(
#         default=1
#         )

#     def __str__(self):
#         return str(self.importance_level)


# 記事クラス
class Article(models.Model):
    tel = models.BooleanField(
        verbose_name="TEL",
        default=False,
    )

    name_list = (
        ('山形','山形'),
        ('新田','新田'),
        ('ユズリハ','ユズリハ'),
        ('三谷','三谷'),  
        ('西村','西村'),
        ('木村','木村'),
        ('片岡','片岡'),
        ('川中','川中'),
        ('中尾','中尾'),
        ('志田','志田'),   
        ('小林','小林'),
        ('加島','加島'),      
    )
    name = models.CharField(
        verbose_name="入力者",
        max_length=100,
        choices=name_list,
        # blank=True, 
        # null=True,
        )

    title = models.CharField(
        verbose_name="タイトル",
        max_length=255,
        blank=True, 
        null=True,
        )
    content = models.TextField(
        verbose_name='詳細',
        blank=True, 
        null=True,
        )
    created_at = models.DateTimeField(
        verbose_name='日時',
        auto_now_add = True,
        blank=True, 
        null=True,
        editable = True,
        )

    updated_at = models.DateTimeField(
        auto_now = True,
        blank = True,
        null = True,
    )

    start_at = models.DateTimeField(
        verbose_name='開始日時',
        # default = datetime.now(),
        blank = True,
        null = True,
    )

    end_at = models.DateTimeField(
        verbose_name='終了日時',
        # default = datetime.now(),
        blank = True,
        null = True,
    )

    memo = models.TextField(
        verbose_name= "メモ",
        blank = True,
        null = True,
    )

    category_list = (
        ('組織問い合わせ','組織問い合わせ'),
        ('細胞診問い合わせ','細胞診問い合わせ'),
        ('連絡','連絡'),
        ('その他','その他'),       
    )
    category = models.CharField(
        verbose_name="カテゴリ",
        max_length=255,
        choices=category_list,
        # blank=True, 
        # null=True,
        )

    importance_list = (
        (1,'1'),
        (2,'2'),
        (3,'3'),
        (4,'4'),    
        (5,'5'),     
    )
    importance_level = models.IntegerField(
        verbose_name="重要度",
        choices=importance_list,
        blank=True, 
        null=True,
        )

    # cate = models.ForeignKey(
    #     Category,on_delete=models.CASCADE
    #     )
    # importance = models.ForeignKey(
    #     Importance, on_delete=models.CASCADE
    #     )
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.start_at = datetime.now()
            self.end_at = datetime.now()
        return super(Article, self).save(*args, **kwargs)


    def __str__(self):
        return str(self.id) +':'+ str(self.title) + '(' + str(self.created_at) + ')'

    class Meta:
        ordering = ('-start_at',)

