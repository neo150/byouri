from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='kirokubo_index'),
    path('<int:num>', views.index, name='kirokubo_index'),
    path('edit/<int:num>', views.edit, name='kirokubo_edit'),
]