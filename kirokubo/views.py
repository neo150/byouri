from django.shortcuts import render
from django.shortcuts import redirect
from .models import Article
from .models import Tag
from .forms import ArticleForm
from .forms import ArticleEditForm
from datetime import date
from datetime import datetime
from django.core.paginator import Paginator

def index(request,num=1):
    # 前方一致で日付をフィルタ
    tel_count = Article.objects.filter(start_at__icontains=date.today()).filter(tel=1).count()
    
    data = Article.objects.all()
    page = Paginator(data, 20)
    params = {
        # 'message':'post前',
        'ArticleForm': ArticleForm(),
        'data':page.get_page(num),
        'tel_count':tel_count,
    }

    if (request.method == 'POST'):
        params = { 'message':'post後'}
        obj = Article()
        article = ArticleForm(request.POST, instance=obj)
        article.save()
        return redirect(to='/kirokubo')

    return render(request, 'kirokubo/index.html', params)

def edit(request, num):
    obj = Article.objects.get(id=num)

    if (request.method == 'POST'):
        article = ArticleForm(request.POST, instance=obj)
        article.save()
        return redirect(to='/kirokubo')

    params = {
        # 'message':'post前',
        'id':num,
        'form': ArticleForm(instance=obj),
    }

    return render(request, 'kirokubo/edit.html', params)

