from django import forms
from .models import Article, Tag

#Articleのフォーム
class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['name','category','tel','importance_level','title','content','memo','start_at','end_at']
        # widgets = {
        #     'importance_level': forms.RadioSelect()
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'
        self.fields['end_at'].widget.attrs['id'] = 'endbox'

# Editフォーム
class ArticleEditForm(forms.ModelForm):
    class Mata:
        model = Article
        fields = ['name','category', 'importance_level', 'title', 'content', 'memo']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control  form-control-lg'